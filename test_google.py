import pytest
from selenium.webdriver.chrome.webdriver import WebDriver


@pytest.allure.title('Ищем Милонова')
def test_google_search():
    with pytest.allure.step('Создаем дарйвер'):
        driver = WebDriver(executable_path='D://selenium//chromedriver.exe')
    with pytest.allure.step('Переходим на google'):
        driver.get('https://www.google.ru')
    with pytest.allure.step('Вводим в поиск Милонов'):
        search_input = driver.find_element_by_xpath('//input[@name="q"]')
        search_input.send_keys('Милонов')
        logo_img = driver.find_element_by_xpath('//img[@id="hplogo"]')
        logo_img.click()
        find_btn = driver.find_element_by_xpath('//input[@name="btnK"]')
    with pytest.allure.step('Жмем найти'):
        find_btn.click()
    results = driver.find_elements_by_xpath('//div[@class="g"]')

    with pytest.allure.step('Ищем ссылку на википедию'):
        for element in results:
            link = element.find_element_by_xpath('.//h3[@class="r"]/a')
            href = link.get_attribute('href')
            if 'wikipedia' in href:
                link.click()
                break
        else:
            raise AssertionError('Не найдена ссылка на википедию')
    with pytest.allure.step('Проверяем Title'):
        driver.switch_to.window(driver.window_handles[1])
        assert driver.title == 'Милонов, Виталий Валентинович — Википедия'
